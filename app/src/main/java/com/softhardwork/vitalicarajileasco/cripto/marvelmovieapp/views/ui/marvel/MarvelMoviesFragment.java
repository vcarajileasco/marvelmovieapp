package com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.views.ui.marvel;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.snackbar.Snackbar;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.R;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.data.Movie;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.presenters.contract.MarvelMoviesPresenter;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.presenters.impl.MarvelMoviesPresenterImpl;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.views.MovieDetailsActivity;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.views.adapters.MovieAdapter;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.views.contract.MarvelMoviesView;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/***************************************************************************************************
 * Marvel Movies Fragment implementation
 */
public class MarvelMoviesFragment extends Fragment implements MarvelMoviesView {

    // view controls
    @BindView(R.id.swipe_refresh) SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.movies_list) GridView movies_list;

    // tabs section number
    private static final String ARG_SECTION_NUMBER = "section_number";

    // interface data
    private PageViewModel pageViewModel;
    private MovieAdapter movieAdapter;

    // presenter data
    private MarvelMoviesPresenter presenter;
    // movies list
    private List<Movie> items;
    // current active tab: 1 - popular; 2 - top rated
    private int showType = 0;


    /***********************************************************************************************
     * fragment instance
     */
    public static MarvelMoviesFragment newInstance(int index) {
        MarvelMoviesFragment fragment = new MarvelMoviesFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    /***********************************************************************************************
     * fragment onCreate function
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }

        pageViewModel.setIndex(index);

        // attach to view presenter
        presenter = new MarvelMoviesPresenterImpl(getContext());
        presenter.onAttach(this);
    }

    /***********************************************************************************************
     * fragment onCreateView function
     */
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // inflate layout
        View root = inflater.inflate(R.layout.fragment_marvel_movies, container, false);
        // initialize interface controls
        ButterKnife.bind(this, root);

        // initialize listener for tabs
        pageViewModel.getIndex().observe(getActivity(), s -> showType = s);

        // initialize movie adapter
        movieAdapter = new MovieAdapter(getContext(), this.items);
        movies_list.setAdapter(movieAdapter);

        // initialize movies list item click
        movies_list.setOnItemClickListener(gridViewOnItemClickListener);

        // initialize movies list refresh swipe
        swipeRefresh.setOnRefreshListener(() -> presenter.getMarvelMovies());

        return root;
    }

    /***********************************************************************************************
     * movies list item click listener
     */
    private GridView.OnItemClickListener gridViewOnItemClickListener = new GridView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

            // initialize MovieDetailsActivity intent
            Intent i = new Intent(getContext(), MovieDetailsActivity.class);
            // sending movie id to MovieDetailsActivity
            i.putExtra("movie_id", items.get(position).getId());
            startActivity(i);
        }
    };

    /***********************************************************************************************
     * show Marvel movies in view
     */
    @Override
    public void showMarvelMovies(List<Movie> movies) {
        this.items = movies;

        Activity activity = getActivity();

        if(activity != null) {
            // run in UI background
            activity.runOnUiThread(() -> {

                // select specific tab
                if(showType == 1) {
                    // sort by popularity
                    Collections.sort(items, (obj1, obj2) -> {

                        Float pop1 = obj2.getPopularity();
                        Float pop2 = obj1.getPopularity();
                        return Integer.compare(pop1.compareTo(pop2), 0);
                    });
                }
                else if(showType == 2) {
                    // sort by rate
                    Collections.sort(items, (obj1, obj2) -> {

                        Float rate1 = obj2.getRate();
                        Float rate2 = obj1.getRate();
                        return Integer.compare(rate1.compareTo(rate2), 0);
                    });
                }

                // update GridView items
                movieAdapter.updateItems(this.items);
            });
        }
    }

    /***********************************************************************************************
     * show data error
     */
    @Override
    public void showDataError() {

        Activity activity = getActivity();
        if(activity == null)
            return;

        // run in UI background
        activity.runOnUiThread(() -> {

            View view = getView();
            if(view == null)
                return;

            // show error message
            Snackbar snackbar = Snackbar.make(view, getString(R.string.error_get_data), Snackbar.LENGTH_LONG)
                    .setAction("Action", null);
            View snackBarView = snackbar.getView();
            snackBarView.setBackgroundColor(getResources().getColor(R.color.colorError));

            snackbar.show();
        });
    }

    /***********************************************************************************************
     * show loading process
     */
    @Override
    public void showLoadingProcess(boolean enable) {
        Activity activity = getActivity();
        if(activity == null)
            return;

        // run in UI background
        activity.runOnUiThread(() -> {
            if(swipeRefresh != null)
                // show refresh animation
                swipeRefresh.setRefreshing(enable);
        });
    }

}