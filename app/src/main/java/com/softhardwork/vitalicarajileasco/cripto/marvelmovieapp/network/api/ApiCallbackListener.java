package com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.network.api;


/***********************************************************************************************
 * Retrofit response callback interface
 */
public interface ApiCallbackListener<T> {
    void onResponse(T data);
    void onFailure();
}
