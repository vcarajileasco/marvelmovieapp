package com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/***********************************************************************************************
 * The Movie Database API: MoviesList class
 */
public class MoviesList {

    /***********************************************************************************************
     * Used parameters in APP
     */
    @SerializedName("items")
    @Expose
    Movie[] items;


    /***********************************************************************************************
     * Unused parameters, but existed in API response - can be deleted
     */
    @SerializedName("created_by")
    @Expose
    String created_by;

    @SerializedName("description")
    @Expose
    String description;

    @SerializedName("favorite_count")
    @Expose
    int favorite_count;

    @SerializedName("id")
    @Expose
    String id;

    @SerializedName("item_count")
    @Expose
    int item_count;

    @SerializedName("iso_639_1")
    @Expose
    String iso_639_1;

    @SerializedName("name")
    @Expose
    String name;

    @SerializedName("poster_path")
    @Expose
    String poster_path;



    /***********************************************************************************************
     * Getters
     */
    public List<Movie> getMovies() {
        if(items == null)
            return new ArrayList<>();
        else
            return Arrays.asList(items);
    }
}
