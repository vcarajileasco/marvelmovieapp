package com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.views.ui.marvel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;


/***************************************************************************************************
 * Page View Model class
 */
public class PageViewModel extends ViewModel {

    private MutableLiveData<Integer> mIndex = new MutableLiveData<>();

    private LiveData<Integer> mValue = Transformations.map(mIndex, input -> input);

    public void setIndex(int index) {
        mIndex.setValue(index);
    }

    public LiveData<Integer> getIndex() {
        return mValue;
    }
}