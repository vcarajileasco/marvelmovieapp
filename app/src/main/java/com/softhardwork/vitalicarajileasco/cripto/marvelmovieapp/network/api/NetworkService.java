package com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.network.api;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/***********************************************************************************************
 * Retrofit Network Service class
 */
public class NetworkService {

    // The Movie Database API urls
    private static final String BASE_URL = "https://api.themoviedb.org";

    public static final String API_KEY = "dad8a59d86a2793dda93aa485f7339c1";
    public static final String IMAGES_URL = "https://image.tmdb.org/t/p/w185";

    // The Movie Database API KEY
    public static final String IMAGES_ORIGINAL_URL = "https://image.tmdb.org/t/p/original";


    // Network Instance params
    private static NetworkService mInstance;
    private Retrofit mRetrofit;

    private NetworkService() {
        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static NetworkService getInstance() {
        if (mInstance == null) {
            mInstance = new NetworkService();
        }
        return mInstance;
    }

    public Api getApi() {
        return mRetrofit.create(Api.class);
    }

}
