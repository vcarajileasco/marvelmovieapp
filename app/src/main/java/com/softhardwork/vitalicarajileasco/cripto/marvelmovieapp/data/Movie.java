package com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.network.api.NetworkService;


/***********************************************************************************************
 * The Movie Database API: Movie class
 */
public class Movie {

    /***********************************************************************************************
    * Used parameters in APP
     */
    @SerializedName("id")
    @Expose
    int id;

    @SerializedName("title")
    @Expose
    String title;

    @SerializedName("vote_average")
    @Expose
    Float vote_average;

    @SerializedName("backdrop_path")
    @Expose
    String backdrop_path;

    @SerializedName("poster_path")
    @Expose
    String poster_path;

    @SerializedName("popularity")
    @Expose
    Float popularity;


    /***********************************************************************************************
    * Unused parameters, but existed in API response - can be deleted
     */
    @SerializedName("vote_count")
    @Expose
    int vote_count;

    @SerializedName("video")
    @Expose
    boolean video;

    @SerializedName("media_type")
    @Expose
    String media_type;

    @SerializedName("adult")
    @Expose
    boolean adult;

    @SerializedName("original_language")
    @Expose
    String original_language;

    @SerializedName("original_title")
    @Expose
    String original_title;

    @SerializedName("overview")
    @Expose
    String overview;

    @SerializedName("release_date")
    @Expose
    String release_date;



    /***********************************************************************************************
    * Getters
     */
    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Float getPopularity() {
        return popularity;
    }

    public Float getRate() {
        return vote_average;
    }

    public String getPosterUrl() {
        return NetworkService.IMAGES_URL + poster_path;
    }

}
