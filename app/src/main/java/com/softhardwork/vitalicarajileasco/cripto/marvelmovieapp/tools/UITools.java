package com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.tools;

import android.os.Build;
import android.os.LocaleList;
import android.widget.ImageView;

import androidx.annotation.DrawableRes;

import com.squareup.picasso.Picasso;

import java.util.Locale;


/***********************************************************************************************
 * Additional app functionality
 */
public class UITools {

    /***********************************************************************************************
     * loading image inside ImageView using Picasso library
     */
    public static void loadImage(String url, @DrawableRes int placeholder, ImageView imageView) {
        if(url != null && !url.isEmpty()) {
            Picasso.get()
                    .load(url)
                    .into(imageView);
        } else {
            if(placeholder != 0)
                imageView.setImageResource(placeholder);
        }
    }

    /***********************************************************************************************
     * get current system language
     */
    public static String getCurrentLanguage(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            return LocaleList.getDefault().get(0).getLanguage();
        } else{
            return Locale.getDefault().getLanguage();
        }
    }

}
