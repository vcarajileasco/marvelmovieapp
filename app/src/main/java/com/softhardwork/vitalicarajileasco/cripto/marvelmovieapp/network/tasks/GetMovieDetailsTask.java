package com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.network.tasks;

import androidx.annotation.NonNull;

import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.data.MovieDetails;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.network.api.ApiCallbackListener;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.network.api.NetworkService;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.tools.UITools;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/***********************************************************************************************
 * Get Movie Details Task
 */
public class GetMovieDetailsTask {

    @Inject
    public GetMovieDetailsTask() {
    }

    public void request(int movieId, ApiCallbackListener<MovieDetails> callback) {
        // get current System Language
        String lang = UITools.getCurrentLanguage();

        // execute request
        NetworkService.getInstance()
                .getApi()
                .getMovieDetails(movieId, NetworkService.API_KEY, lang)
                .enqueue(new Callback<MovieDetails>() {

                    // request - success
                    @Override
                    public void onResponse(@NonNull Call<MovieDetails> call, @NonNull Response<MovieDetails> response) {

                        if(response.body() == null) {
                            callback.onFailure();
                        } else {
                            MovieDetails data = response.body();
                            callback.onResponse(data);
                        }
                    }

                    // request - error
                    @Override
                    public void onFailure(@NonNull Call<MovieDetails> call, @NonNull Throwable t) {

                        t.printStackTrace();
                        callback.onFailure();
                    }
                });
    }

}
