package com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.presenters.contract;

import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.views.contract.MarvelMoviesView;


/***********************************************************************************************
 * Marvel Movies Presenter interface
 */
public interface MarvelMoviesPresenter {
    // attach view to presenter
    void onAttach(MarvelMoviesView view);
    // request Marvel movies list
    void getMarvelMovies();
}