package com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.network.api;

import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.data.MovieDetails;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.data.MoviesList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


/***********************************************************************************************
 * The Movie Database API: Requests list
 */
public interface Api {

    /***********************************************************************************************
     * Request used to get Marvel movies list
     */
    @GET("/3/list/1")
    public Call<MoviesList> getMarvelMovies(@Query("api_key") String api_key, @Query("language") String language);

    /***********************************************************************************************
     * Request used to get movie details
     */
    @GET("/3/movie/{movie_id}")
    public Call<MovieDetails> getMovieDetails(@Path("movie_id") int movie_id, @Query("api_key") String api_key, @Query("language") String language);


    /***********************************************************************************************
     * Requests used to get popular movies - not used in current App
     */
    @GET("/3/movie/popular")
    public Call<MoviesList> getPopularMovies(@Query("api_key") String api_key, @Query("language") String language);

    /***********************************************************************************************
     * Requests used to get top rated movies - not used in current App
     */
    @GET("/3/movie/top_rated")
    public Call<MoviesList> getTopRatedMovies(@Query("api_key") String api_key, @Query("language") String language);
}
