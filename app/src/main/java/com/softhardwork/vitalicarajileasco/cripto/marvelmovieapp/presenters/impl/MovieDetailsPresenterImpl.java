package com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.presenters.impl;

import android.content.Context;

import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.data.MovieDetails;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.network.api.ApiCallbackListener;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.network.tasks.GetMovieDetailsTask;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.presenters.contract.MovieDetailsPresenter;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.views.contract.MovieDetailsView;

import javax.inject.Inject;


/***********************************************************************************************
 * Movie Details Presenter implementation
 */
public class MovieDetailsPresenterImpl implements MovieDetailsPresenter {

    // child view
    private MovieDetailsView view;

    // movie's details task
    private GetMovieDetailsTask getMovieDetailsTask;

    /***********************************************************************************************
     * constructor
     */
    @Inject
    public MovieDetailsPresenterImpl(Context context) {
        this.getMovieDetailsTask = new GetMovieDetailsTask();
    }

    /***********************************************************************************************
     * attach view to presenter
     */
    @Override
    public void onAttach(MovieDetailsView view) {
        this.view = view;
    }

    /***********************************************************************************************
     * request movie's details
     */
    @Override
    public void getMovieDetails(int movieId) {
        view.showLoadingProcess(true);

        // get request
        getMovieDetailsTask.request(movieId, new ApiCallbackListener<MovieDetails>() {

            @Override
            public void onResponse(MovieDetails data) {
                view.showLoadingProcess(false);

                if( data != null) {
                    view.showMovieDetails(data);
                }
                else {
                    view.showDataError();
                }
            }

            @Override
            public void onFailure() {
                view.showLoadingProcess(false);
                view.showDataError();
            }
        });
    }


}
