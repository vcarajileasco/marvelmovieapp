package com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.presenters.contract;

import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.views.contract.MovieDetailsView;


/***********************************************************************************************
 * Movie Details Presenter interface
 */
public interface MovieDetailsPresenter {
    // attach view to presenter
    void onAttach(MovieDetailsView view);
    // request movie's details
    void getMovieDetails(int movieId);
}