package com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.views;


import android.content.Intent;
import android.os.Bundle;
import android.widget.ProgressBar;

import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.R;

import butterknife.BindView;


/***************************************************************************************************
 * Loading Activity
 */
public class LoadingActivity extends BaseActivity {

    // view controls
    @BindView(R.id.progress_bar) ProgressBar progressBar;


    /***********************************************************************************************
     * onCreate function
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
    }

    /***********************************************************************************************
     * setContentView function
     */
    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);

        // color ProgressBar
        progressBar.getIndeterminateDrawable().setColorFilter(0xFFcc0000, android.graphics.PorterDuff.Mode.SRC_ATOP);

        // open movie list after delay
        startApplicationAfterDelay();
    }

    /***********************************************************************************************
     * create MarvelMoviesActivity Intent and open Activity
     */
    private void startApplicationAfterDelay() {
        Intent intent = new Intent(getApplicationContext(), MarvelMoviesActivity.class);
        openActivityAfterDelayAndFinish(3000, intent);
    }

}