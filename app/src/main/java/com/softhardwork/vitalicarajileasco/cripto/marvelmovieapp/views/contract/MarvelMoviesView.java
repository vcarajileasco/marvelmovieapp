package com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.views.contract;

import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.data.Movie;

import java.util.List;


/***********************************************************************************************
 * Marvel Movies View interface
 */
public interface MarvelMoviesView {
    // show Marvel movies in view
    void showMarvelMovies(List<Movie> movies);
    // show data error
    void showDataError();
    // show loading process
    void showLoadingProcess(boolean enable);
}
