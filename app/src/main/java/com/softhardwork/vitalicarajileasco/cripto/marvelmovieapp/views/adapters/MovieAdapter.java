package com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.R;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.data.Movie;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.tools.UITools;

import java.util.List;

/***********************************************************************************************
 * Movie Item Adapter
 */
public class MovieAdapter extends BaseAdapter {

    // parent context
    private final Context mContext;
    // movies list
    private List<Movie> items;


    /***********************************************************************************************
     * constructor
     */
    public MovieAdapter(Context context, List<Movie> items) {
        this.mContext = context;
        this.items = items;
    }

    /***********************************************************************************************
     * get Items count
     */
    @Override
    public int getCount() {
        if(items == null)
            return 0;
        else
            return items.size();
    }

    /***********************************************************************************************
     * get Item position
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /***********************************************************************************************
     * get specific Item by position
     */
    @Override
    public Object getItem(int position) {
        if(items == null || position > items.size())
            return null;
        else
            return items.get(position);
    }

    /***********************************************************************************************
     * get Item view with content
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View grid;

        // try to reuse existing grid cell views
        if (convertView == null) {

            // create new grid cell view
            grid = new View(mContext);
            //LayoutInflater inflater = getLayoutInflater();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
            grid = inflater.inflate(R.layout.cell_movie, parent, false);
        } else {

            // use existing grid cell view
            grid = (View) convertView;
        }

        // read View controls from Movie cell
        ImageView imageView = (ImageView) grid.findViewById(R.id.movie_poster);
        TextView textView = (TextView) grid.findViewById(R.id.movie_title);

        // get movie object
        Movie movie = items.get(position);

        // complete movie information
        UITools.loadImage(movie.getPosterUrl(), 0, imageView);
        textView.setText(movie.getTitle());

        // return completed view
        return grid;
    }

    /***********************************************************************************************
     * update GridView items list
     */
    public void updateItems(List<Movie> items) {
        this.items = items;
        notifyDataSetChanged();
    }


}