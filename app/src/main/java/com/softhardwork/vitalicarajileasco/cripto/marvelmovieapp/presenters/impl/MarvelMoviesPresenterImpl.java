package com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.presenters.impl;

import android.content.Context;

import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.data.MoviesList;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.network.api.ApiCallbackListener;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.network.tasks.GetMarvelMoviesTask;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.presenters.contract.MarvelMoviesPresenter;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.views.contract.MarvelMoviesView;

import javax.inject.Inject;


/***********************************************************************************************
 * Marvel Movies Presenter implementation
 */
public class MarvelMoviesPresenterImpl implements MarvelMoviesPresenter {

    // child view
    private MarvelMoviesView view;

    // Marvel movies list task
    private GetMarvelMoviesTask getMarvelMoviesTask;

    /***********************************************************************************************
     * constructor
     */
    @Inject
    public MarvelMoviesPresenterImpl(Context context) {
        this.getMarvelMoviesTask = new GetMarvelMoviesTask();
    }

    /***********************************************************************************************
     * attach view to presenter
     */
    @Override
    public void onAttach(MarvelMoviesView view) {
        this.view = view;

        getMarvelMovies();
    }

    /***********************************************************************************************
     * request Marvel movies list
     */
    @Override
    public void getMarvelMovies() {
        view.showLoadingProcess(true);

        // get request
        getMarvelMoviesTask.request(new ApiCallbackListener<MoviesList>() {

            @Override
            public void onResponse(MoviesList data) {
                view.showLoadingProcess(false);

                if( data != null) {
                    view.showMarvelMovies(data.getMovies());
                }
                else {
                    view.showDataError();
                }
            }

            @Override
            public void onFailure() {
                view.showLoadingProcess(false);
                view.showDataError();
            }
        });
    }


}
