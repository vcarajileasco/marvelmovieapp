package com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.views.contract;

import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.data.MovieDetails;


/***********************************************************************************************
 * Movie Details View interface
 */
public interface MovieDetailsView {
    // show movie's details
    void showMovieDetails(MovieDetails movieDetails);
    // show data error
    void showDataError();
    // show loading process
    void showLoadingProcess(boolean enable);
}
