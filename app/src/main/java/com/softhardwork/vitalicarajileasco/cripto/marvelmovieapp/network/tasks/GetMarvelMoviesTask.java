package com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.network.tasks;

import androidx.annotation.NonNull;

import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.data.MoviesList;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.network.api.ApiCallbackListener;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.network.api.NetworkService;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.tools.UITools;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/***********************************************************************************************
 * Get Marvel Movies Task
 */
public class GetMarvelMoviesTask {

    @Inject
    public GetMarvelMoviesTask() {
    }

    public void request(ApiCallbackListener<MoviesList> callback) {
        // get current System Language
        String lang = UITools.getCurrentLanguage();

        // execute request
        NetworkService.getInstance()
                .getApi()
                .getMarvelMovies(NetworkService.API_KEY, lang)
                .enqueue(new Callback<MoviesList>() {

                    // request - success
                    @Override
                    public void onResponse(@NonNull Call<MoviesList> call, @NonNull Response<MoviesList> response) {

                        if(response.body() == null) {
                            callback.onFailure();
                        } else {
                            MoviesList data = response.body();
                            callback.onResponse(data);
                        }
                    }

                    // request - error
                    @Override
                    public void onFailure(@NonNull Call<MoviesList> call, @NonNull Throwable t) {

                        t.printStackTrace();
                        callback.onFailure();
                    }
                });
    }

}
