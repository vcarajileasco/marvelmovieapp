package com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.network.api.NetworkService;

import java.text.DecimalFormat;


/***********************************************************************************************
 * The Movie Database API: MovieDetails class
 */
public class MovieDetails {

    /***********************************************************************************************
     * Used parameters in APP
     */
    @SerializedName("adult")
    @Expose
    boolean adult;

    @SerializedName("id")
    @Expose
    int id;

    @SerializedName("backdrop_path")
    @Expose
    String backdrop_path;

    @SerializedName("homepage")
    @Expose
    String homepage;

    @SerializedName("overview")
    @Expose
    String overview;

    @SerializedName("release_date")
    @Expose
    String release_date;

    @SerializedName("runtime")
    @Expose
    int runtime;

    @SerializedName("title")
    @Expose
    String title;

    @SerializedName("vote_average")
    @Expose
    Float vote_average;


    /***********************************************************************************************
     * Unused parameters, but existed in API response - can be deleted
     */
    @SerializedName("popularity")
    @Expose
    Float popularity;

    @SerializedName("poster_path")
    @Expose
    String poster_path;

    @SerializedName("budget")
    @Expose
    int budget;

    @SerializedName("original_language")
    @Expose
    String original_language;

    @SerializedName("original_title")
    @Expose
    String original_title;

    @SerializedName("revenue")
    @Expose
    int revenue;

    @SerializedName("status")
    @Expose
    String status;

    @SerializedName("tagline")
    @Expose
    String tagline;

    @SerializedName("vote_count")
    @Expose
    int vote_count;



    /***********************************************************************************************
     * Getters
     */
    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getBackdropUrl() {
        return NetworkService.IMAGES_ORIGINAL_URL + backdrop_path;
    }

    public String getYear() {
        return release_date;
    }

    public String getRate() {
        return new DecimalFormat("##.####").format(vote_average);
    }

    public int getRuntime() {
        return runtime;
    }

    public String getOverview() {
        return overview;
    }

    public String getHomepage() {
        return homepage;
    }

}
