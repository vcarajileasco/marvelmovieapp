package com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.views;

import android.os.Bundle;

import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.R;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.views.ui.marvel.SectionsPagerAdapter;

import butterknife.BindView;


/***************************************************************************************************
 * Marvel Movies Activity
 */
public class MarvelMoviesActivity extends BaseActivity {

    // view controls
    @BindView(R.id.view_pager) ViewPager viewPager;
    @BindView(R.id.tabs) TabLayout tabs;


    /***********************************************************************************************
     * onCreate function
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marvel_movies);
    }

    /***********************************************************************************************
     * onCreate function
     */
    @Override
    public void onBackPressed() {
        //super.onBackPressed();

        // hide application after press back button
        moveTaskToBack(true);
    }

    /***********************************************************************************************
     * setContentView function
     */
    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);

        // get sectionsPagerAdapter
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());

        // init view controls
        viewPager.setAdapter(sectionsPagerAdapter);
        tabs.setupWithViewPager(viewPager);
    }
}