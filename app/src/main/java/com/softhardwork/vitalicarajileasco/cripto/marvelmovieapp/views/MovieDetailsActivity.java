package com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.views;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.R;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.data.MovieDetails;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.presenters.contract.MovieDetailsPresenter;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.presenters.impl.MovieDetailsPresenterImpl;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.tools.UITools;
import com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.views.contract.MovieDetailsView;

import butterknife.BindView;


/***************************************************************************************************
 * Movie Details Activity
 */
public class MovieDetailsActivity extends BaseActivity implements MovieDetailsView {

    // view controls
    @BindView(R.id.fab_browser) FloatingActionButton browserButton;
    @BindView(R.id.details_toolbar) Toolbar detailsToolbar;
    @BindView(R.id.toolbar_layout) CollapsingToolbarLayout toolbarLayout;
    @BindView(R.id.image_toolbar) ImageView imageToolbar;

    @BindView(R.id.movie_overview) TextView movieOverview;


    // view presenter
    private MovieDetailsPresenter presenter;

    // data
    private int movieId;
    private MovieDetails movieDetails;


    /***********************************************************************************************
     * onCreate function
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
    }

    /***********************************************************************************************
     * setContentView function
     */
    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);

        // read input param - movie_id
        Intent intent = getIntent();
        movieId = intent.getIntExtra("movie_id", -1);

        // check if input param is wrong
        if(movieId == -1) {
            // wrong data, exit from activity
            finish();
        }
        else {
            // initialize rest of view
            setSupportActionBar(detailsToolbar);
            enableBackButton();

            // initialize browser info button click
            browserButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(movieDetails != null)
                        openBrowser(movieDetails.getHomepage());
                }
            });

            // attach to view presenter
            presenter = new MovieDetailsPresenterImpl(this);
            presenter.onAttach(this);

            // get movie's details
            presenter.getMovieDetails(movieId);
        }
    }

    /***********************************************************************************************
     * inflate activity menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_details, menu);
        return true;
    }

    /***********************************************************************************************
     * check pressed activity menu item
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        // check if pressed reload content menu item
        if (id == R.id.action_reload) {
            presenter.getMovieDetails(movieId);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /***********************************************************************************************
     * show movie's details
     */
    @Override
    public void showMovieDetails(MovieDetails movieDetails) {
        this.movieDetails = movieDetails;

        if(movieDetails != null) {

            // run in UI background
            runOnUiThread(() -> {
                // set movie title
                toolbarLayout.setTitle(movieDetails.getTitle());

                // set movie image
                UITools.loadImage(movieDetails.getBackdropUrl(), 0, imageToolbar);

                // set movie description
                movieOverview.setText(getString(R.string.movie_overview,
                        movieDetails.getTitle(),
                        movieDetails.getYear(),
                        movieDetails.getRate(),
                        movieDetails.getRuntime(),
                        movieDetails.getOverview()));
            });
        }
    }

    @Override
    public void showDataError() {
        // run in UI background
        runOnUiThread(() -> {

            // show error message
            Snackbar snackbar = Snackbar.make(findViewById(R.id.details_activity), getString(R.string.error_get_data), Snackbar.LENGTH_LONG)
                    .setAction("Action", null);
            View snackBarView = snackbar.getView();
            snackBarView.setBackgroundColor(getResources().getColor(R.color.colorError));

            snackbar.show();
        });
    }

    @Override
    public void showLoadingProcess(boolean enable) {
    }
}
