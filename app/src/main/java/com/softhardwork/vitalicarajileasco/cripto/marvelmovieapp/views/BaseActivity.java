package com.softhardwork.vitalicarajileasco.cripto.marvelmovieapp.views;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;

import androidx.annotation.LayoutRes;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import butterknife.ButterKnife;

/***************************************************************************************************
 * General Base Activity
 */
public class BaseActivity extends AppCompatActivity {

    // activity data
    private Bundle savedInstanceState;
    private boolean backButtonEnabled = false;

    /***********************************************************************************************
     * BaseActivity onCreate function
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.savedInstanceState = savedInstanceState;
    }

    /***********************************************************************************************
     * BaseActivity setContentView function
     */
    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);

        // initialize interface controls
        ButterKnife.bind(this);

        // init view interface
        initView(savedInstanceState);
    }

    /***********************************************************************************************
     * BaseActivity setContentView function
     */
    protected void initView(Bundle savedInstanceState) {
    }

    /***********************************************************************************************
     * BaseActivity menu onOptionsItemSelected function
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)     {
        switch (item.getItemId()) {
            case android.R.id.home:
                if(!backButtonEnabled) break;
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /***********************************************************************************************
     * BaseActivity show back arrow button
     */
    protected void enableBackButton() {
        try {
            ActionBar actionBar = getSupportActionBar();
            if( actionBar != null)
                actionBar.setDisplayHomeAsUpEnabled(true);
            backButtonEnabled = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***********************************************************************************************
     * open new Activity after delay and close current Activity
     */
    protected void openActivityAfterDelayAndFinish(int delay_ms, Intent intent) {
        if(intent != null) {
            new Handler().postDelayed(() -> {
                //Do something after 500ms
                openActivityAndFinish(intent);
            }, delay_ms);
        }
    }

    /***********************************************************************************************
     * open new Activity and close current Activity
     */
    protected void openActivityAndFinish(Intent intent) {

        // run in UI background
        this.runOnUiThread(() -> {
            startActivity(intent);

            finish();
        });
    }

    /***********************************************************************************************
     * open url in system Browser
     */
    public void openBrowser(String url) {
        if(url == null || url.isEmpty())
            return;

        // open browsers
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

}
